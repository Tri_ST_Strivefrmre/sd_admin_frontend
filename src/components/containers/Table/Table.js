import React, { Component} from 'react';
import ReactTable from 'react-table-6';
import "react-table-6/react-table.css";


class Table extends Component{

    render(){     
        return (
            <ReactTable
            columns={this.props.columns}
            data={this.props.data}
            className="-striped -highlight"
            defaultPageSize={10}
            SubComponent={this.props.subComponent}
            >

            </ReactTable>
        )
    }
}

export default Table;