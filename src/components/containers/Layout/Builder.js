import React, { Component} from 'react';
import CandidateData from '../../../data/sample-example.json';
import Table from '../Table/Table';
import Star from '../../Controls/Star';
import Card from '../../Controls/Card';

class Builder extends Component{
    
    state = {
        candidateDetails:[],
        candidateProfile:[]
 
     }
     componentDidMount(){
         let candidateProfile = [];
         this.setState({candidateDetails: CandidateData});
         CandidateData.map((candidate, index)=>{
            candidateProfile = [...candidateProfile,
                 {
                     'real_name': candidate.real_name,
                     'years_as_sw_dev': candidate.years_as_sw_dev,
                     'favourite_language': candidate.favourite_language,
                     'australian_citizen': candidate.australian_citizen,
                     'star': <Star id={index} default={false} />
                 }
             ]
         })
         this.setState({candidateProfile: candidateProfile})
     }

     render(){
        const columns = [{
            Header:' ',
            accessor: 'star',
            width: 80
            },{
            Header: 'Name',
            accessor: 'real_name',
            width: 400
          }, {
            Header: 'Yrs of Experience',
            accessor: 'years_as_sw_dev',
            width: 200
          }, {
            Header: 'Favourite Language',
            accessor: 'favourite_language'
          },
          {
            id: 'australian_citizen',
            Header: 'Australian Citizen',
            accessor: d => {d.australian_citizen!== undefined? d.australian_citizen.toString(): 'No Info'},
            
          }]
         return(
            <div>
                <Table
                columns={columns}
                data={this.state.candidateProfile}
                subComponent={row =>{
                    return (
                        <div style={{alignContent: 'left',padding:'8rem' }}>
                           {/* <Card.Body style={{ alignContent: 'left' }}>
                                {this.state.candidateDetails[row.index].real_name}
                    </Card.Body> */}
                    <Card width = {'100rem'}
                    src={this.state.candidateDetails[row.index].photo}
                    title={this.state.candidateDetails[row.index].real_name}
                    description={this.state.candidateDetails[row.index].description}
                    items={[
                        {'key':'Citizen',
                        'value':this.state.candidateDetails[row.index].australian_citizen!== undefined? 
                                this.state.candidateDetails[row.index].australian_citizen : 'No Info'  },
                        {'key':'Date of Birth','value': this.state.candidateDetails[row.index].dob.substring(0, 10)},
                        {'key':'Favourite Language','value':this.state.candidateDetails[row.index].favourite_language}

                    ]}
                    contentType={this.state.candidateDetails[row.index].resume_content_type}
                    data={this.state.candidateDetails[row.index].resume_base64}
                    >
                    </Card>
                        </div>
                    );
                }}
                />
            </div>
         )
     }
}

export default Builder;