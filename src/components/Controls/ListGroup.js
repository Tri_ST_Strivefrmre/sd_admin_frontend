import React from 'react';
import {ListGroup} from 'react-bootstrap';
import ListGroupItem from './ListGroupItem';

const ListGroupData = (props) =>{
    
    let listItems = props.items.map((item, index)=>{
        return <ListGroupItem key={index}>
                    {item.key}: {item.value}
                </ListGroupItem>
    })
    return(
        <div>
            <ListGroup className="list-group-flush">
                {listItems}
            </ListGroup>
      </div>
    )
}

export default ListGroupData;

