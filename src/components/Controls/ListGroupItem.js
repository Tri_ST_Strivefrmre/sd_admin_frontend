import React from 'react';
import { ListGroupItem } from 'react-bootstrap';

const ListGroupDataItem = (props) =>{

    return(
    <ListGroupItem>{props.children}</ListGroupItem>
    )
}

export default ListGroupDataItem;