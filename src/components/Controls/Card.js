import React from 'react';
import { Card} from 'react-bootstrap';
import EmbededMedia from './EmbededMedia';
import ListGroupData from './ListGroup';
const ProfileCard = (props) =>{

    return(
        <Card style={{width: props.width}}>
        <Card.Img variant="top" style={{width:'100px', alignSelf:'center'}}  src={props.src}/>
        <Card.Body>
          <Card.Title>{props.title}</Card.Title>
          <Card.Text>
            {props.description}
          </Card.Text>
        </Card.Body>
        <ListGroupData items={props.items}>
        </ListGroupData>
        <br/>
        <br/>
        <h5> Resume </h5>
        <EmbededMedia 
        contentType={props.contentType}
        data={props.data}
        />
      </Card>
    )
}

export default ProfileCard;