import React from 'react';

const EmbededMedia = (props) =>{

    let contentType = props.contentType;
    let data = props.data;
    return(
        <embed style={{overflow:'visible', border:'1px solid', height:'30rem'}} src={'data:'+contentType+';base64,'+data}/>
    )
}

export default EmbededMedia;