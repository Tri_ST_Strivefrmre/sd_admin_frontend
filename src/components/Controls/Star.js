import React, { Component } from 'react';
import { TiStarOutline,TiStarFullOutline } from "react-icons/ti";
import Button from 'react-bootstrap/Button';
class Star extends Component{

    render(){
        return(
            <Button variant='light'
            id = {this.props.id}
            onClick={this.props.clicked}>
               {!this.props.default ? 
                   <TiStarOutline/> : <TiStarFullOutline/>} 
            </Button>
        )
    }
}

export default Star;