import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Builder from './components/containers/Layout/Builder';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Admin Tool for Job Candidates</h1>
        </header>
        <p className="App-intro">
          

        </p>
       <Builder/>
      </div>
    );
  }
}

export default App;
